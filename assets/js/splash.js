var splashState = {
    preload: function () {
        game.load.image('splash', 'assets/img/splashscreen.png');
        game.load.audio('bootSound', 'assets/sound/startup.mp3');
    },

    create: function () {
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.refresh();

        game.state.add('main', mainState);

        this.sound = game.add.audio('bootSound');
        this.sound.volume = .15;

        this.sound.play();

        game.stage.backgroundColor = '000000';

        this.logo = game.add.sprite(game.world.centerX, game.world.centerY, 'splash');
        this.logo.anchor.setTo(0.5, 0.5);
        this.logo.alpha = 0;

        game.add.tween(this.logo).to( { alpha: 1}, 5000, Phaser.Easing.Linear.None, true);

        game.time.events.add(Phaser.Timer.SECOND*8.5, function(){
            this.sound.destroy();
            game.state.start('main');
        }, this);
    }
};

var width = 900;
var height = 600;

// Initialize Phaser
var game = new Phaser.Game(width, height, Phaser.CANVAS);

// Add states
game.state.add('splash', splashState);

// Start the state
game.state.start('splash');