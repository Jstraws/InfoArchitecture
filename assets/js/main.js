var buttons, locations, levers, feedLevel, bars, feedDelay;

var mainState = {
    preload: function () {
        game.load.spritesheet('onOff', 'assets/img/on_off.png', 75, 35);
        game.load.spritesheet('jeep', 'assets/img/jeep.png', 75, 50);
        game.load.spritesheet('jeep_button', 'assets/img/jeep_button.png', 80, 72);
        game.load.spritesheet('lever', 'assets/img/lever.png', 52, 80);
        game.load.image('bg', 'assets/img/background.png');
    },

    create: function () {
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.refresh();

        this.bg = game.add.sprite(0, 0, 'bg');

        buttons = game.add.group();
        buttons.inputEnableChildren = true;


        //************************************************************************************************************\\
        // Creates the electric fence controls.
        //************************************************************************************************************\\

        for(var i = 0; i < 4; i++) {
            var sprite = buttons.create(215, 184+(i*50), 'onOff');
            sprite.name = 'fence' + i;
            sprite.anchor.setTo(0.5, 0.5);
            sprite.input.useHandCursor = true;
            sprite.events.onInputUp.add(this.toggle, this);
        }

        //************************************************************************************************************\\
        // Creates the vehicle tracker and buttons.
        //************************************************************************************************************\\
        locations = ['Triceratops', 'Tyrannosaurus\nRex', 'Velociraptor', 'Stegosaurus'];
        var textStyle;

        for(i = 0; i < 3; i++) {
            var button = buttons.create(game.world.width-295, 184+(i*75), 'jeep_button');
            button.name = 'jButton' + i;
            button.anchor.setTo(0.5, 0.5);
            button.input.useHandCursor = true;
            button.events.onInputUp.add(this.jToggle, this);
            button.frame = 1;

            button.addChild(game.make.sprite(80, 0, 'jeep'));
            var jeep = button.getChildAt(0);
            jeep.name = 'jeep' + i;
            jeep.anchor.setTo(0.5, 0.5);
            jeep.animations.add('move');
            jeep.animations.play('move', 10, true);

            textStyle = { font: '10pt Times New Roman', fill: 'White', align: 'center', stroke: 'rgba(0,0,0,0)', strokeThickness: 4};
            button.addChild(game.make.text(120, 0, locations[game.rnd.integerInRange(0, 3)], textStyle));
            var label =  button.getChildAt(1);
            label.name = 'location' + i;
            label.anchor.setTo(0, 0.4);
            label.lineSpacing = -10;
        }

        //************************************************************************************************************\\
        // Timers to "move" the vehicles
        //************************************************************************************************************\\

        this.timer1 = game.time.create(false);
        this.timer1.loop(Phaser.Timer.SECOND*game.rnd.integerInRange(5, 10), this.updateLocation1, this);
        this.timer1.start();

        this.timer2 = game.time.create(false);
        this.timer2.loop(Phaser.Timer.SECOND*game.rnd.integerInRange(5, 10), this.updateLocation2, this);
        this.timer2.start();

        this.timer3 = game.time.create(false);
        this.timer3.loop(Phaser.Timer.SECOND*game.rnd.integerInRange(5, 10), this.updateLocation3, this);
        this.timer3.start();

        //************************************************************************************************************\\
        // Create the feeding controls
        //************************************************************************************************************\\
        levers = [false, false, false, false];
        feedLevel = [100, 100, 100, 100];
        bars = [];
        
        for(i = 0; i < 4; i++) {
            var lever = buttons.create(155+(i*205), game.world.centerY+195, 'lever');
            lever.name = i;
            lever.anchor.setTo(0.5, 0.5);
            lever.inputEnabled = true;
            lever.input.useHandCursor = true;
            lever.events.onInputDown.add(this.leverToggle, this);
            lever.events.onInputUp.add(this.leverToggle, this);

            // lever.addChild(game.make.text(-30, -80, locations[i], textStyle));
            lever.addChild(game.make.text(0, -60, locations[i], textStyle));
            label = lever.getChildAt(0);
            label.anchor.set(0.5);
            label.lineSpacing = -10;
            label.name = 'feed' + i;

            lever.addChild(game.make.text(0, 30, 'Enough Feed\nFor: ', textStyle));
            label = lever.getChildAt(1);
            label.anchor.setTo(0.5, 0);
            label.lineSpacing = -10;
            label.name = 'feedLevel' + i;


            var barConfig = {
                x: buttons.getByName(i).x-60,
                y: buttons.getByName(i).y+75,
                animationDuration: 5
            };

            var bar = new HealthBar(this.game, barConfig);
            bar.name = 'feedBar' + i;
            bars[i] = bar;
        }

        feedDelay = game.rnd.integerInRange(3, 5);

        game.time.events.loop(Phaser.Timer.SECOND*feedDelay, function () {
            for(var i = 0; i < 4; i++) {
                if(feedLevel[i] > 0) {
                    feedLevel[i] -= game.rnd.integerInRange(2, 5);
                }
            }

        }, this);
    },

    update: function () {
        for(var i = 0; i < 4; i++) {
            bars[i].setPercent(feedLevel[i]);

            if(levers[i]) {
                if(feedLevel[i] < 100) {
                    feedLevel[i] += 0.15;
                }
            }

            buttons.getByName(i).getChildAt(1).text = 'Need Feed\nIn: ' + Math.round(feedLevel[i]/feedDelay) + ' Seconds';
        }
    },

    toggle: function (sprite) {
        if(sprite.frame === 0) {
            sprite.frame = 1;
        } else {
            sprite.frame = 0;
        }
    },

    jToggle: function (sprite) {
        if(sprite.frame === 0) {
            sprite.frame = 1;
            sprite.getChildAt(0).animations.play('move', 10, true);
            if(sprite.name === 'jButton0') {
                this.timer1.resume();
            } else if(sprite.name === 'jButton1') {
                this.timer2.resume();
            } else {
                this.timer3.resume();
            }
        } else {
            sprite.frame = 0;
            sprite.getChildAt(0).animations.stop();
            if(sprite.name === 'jButton0') {
                this.timer1.pause();
            } else if(sprite.name === 'jButton1') {
                this.timer2.pause();
            } else {
                this.timer3.pause();
            }
        }
    },
    
    updateLocation1: function () {
        var label = buttons.getByName('jButton0').getChildAt(1);
        var current = label.text;
        if(current === 'Triceratops') {
            label.text = locations[1];
        } else if(current === 'Tyrannosaurus Rex') {
            label.text = locations[2];
        } else if(current === 'Velociraptor') {
            label.text = locations[3];
        } else {
            label.text = locations[0];
        }
    },

    updateLocation2: function () {
        var label = buttons.getByName('jButton1').getChildAt(1);
        var current = label.text;
        if(current === 'Triceratops') {
            label.text = locations[1];
        } else if(current === 'Tyrannosaurus Rex') {
            label.text = locations[2];
        } else if(current === 'Velociraptor') {
            label.text = locations[3];
        } else {
            label.text = locations[0];
        }
    },

    updateLocation3: function () {
        var label = buttons.getByName('jButton2').getChildAt(1);
        var current = label.text;
        if(current === 'Triceratops') {
            label.text = locations[1];
        } else if(current === 'Tyrannosaurus Rex') {
            label.text = locations[2];
        } else if(current === 'Velociraptor') {
            label.text = locations[3];
        } else {
            label.text = locations[0];
        }
    },

    leverToggle: function (sprite) {
        if(sprite.frame === 0) {
            sprite.frame = 1;
            levers[sprite.name] = true;
        } else {
            sprite.frame = 0;
            levers[sprite.name] = false;
        }
    }
};

var width = 900;
var height = 600;
//
// // Initialize Phaser
// var game = new Phaser.Game(width, height, Phaser.CANVAS);
//
// // Add states
// game.state.add('main', mainState);
//
// // Start the state
// game.state.start('main');
